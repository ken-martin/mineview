/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "mvLobby.h"
#include "mvAudioHandler.h"
#include "mvLobbyClient.h"
#include "mvView.h"

#include <algorithm>
#include <codecvt>
#include <queue>
#include <sstream>

#include "vtksys/CommandLineArguments.hxx"
#include "vtksys/SystemTools.hxx"

#include "vtkActor.h"
#include "vtkCallbackCommand.h"
#include "vtkCamera.h"
#include "vtkCullerCollection.h"
#include "vtkEventData.h"
#include "vtkGlobFileNames.h"
#include "vtkImageData.h"
#include "vtkInformation.h"
#include "vtkInteractorStyleJoystickCamera.h"
#include "vtkJPEGReader.h"
#include "vtkLight.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkOpenGLRenderer.h"
#include "vtkOpenGLTexture.h"
#include "vtkOutputWindow.h"
#include "vtkPlaneSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkSelection.h"
#include "vtkSelectionNode.h"
#include "vtkVRModel.h"


#include "vtkOpenVRInteractorStyle.h"
#include "vtkOpenVRRenderWindow.h"
#include "vtkOpenVRRenderWindowInteractor.h"
#include "vtkOpenVRRenderer.h"

#include "vtkOpenXRInteractorStyle.h"
#include "vtkOpenXRRenderWindow.h"
#include "vtkOpenXRRenderWindowInteractor.h"
#include "vtkOpenXRRenderer.h"

#include "vtkWindows.h"
#include <mmsystem.h>
#include <shobjidl_core.h>

#ifndef M_PI
#define M_PI vtkMath::Pi()
#endif

mvLobby::mvLobby(int argc, char* argv[])
{
  this->RenderWindow = nullptr;
  this->State = Unknown;
  this->ArcContentSize = 38;
  this->ArcGap = 8;
  this->PosterHeight = 2.5;
  this->EventCommand = nullptr;
  this->CurrentView = -1;
  this->PosterCenter[0] = 0.0;
  this->PosterCenter[1] = 0.0;
  this->PosterRadius = 3.5;
  this->Volume = -1.0;
  this->AudioHandler = nullptr;
  this->Client = mvLobbyClient::New();

  this->Camera = nullptr;
  this->Renderer = nullptr;
  this->Interactor = nullptr;
  this->Style = nullptr;

  this->PhysicalPose = new vtkVRCamera::Pose();
  this->PhysicalPose->Distance = 1.0;
  std::fill(this->PhysicalPose->Translation, this->PhysicalPose->Translation + 3, 0.0);
  this->PhysicalPose->PhysicalViewDirection[0] = 0;
  this->PhysicalPose->PhysicalViewDirection[1] = 0;
  this->PhysicalPose->PhysicalViewDirection[2] = -1;
  this->PhysicalPose->PhysicalViewUp[0] = 0;
  this->PhysicalPose->PhysicalViewUp[1] = 1.0;
  this->PhysicalPose->PhysicalViewUp[2] = 0.0;

  bool displayHelp = false;

  typedef vtksys::CommandLineArguments argT;
  argT arguments;
  arguments.Initialize(argc, argv);
  arguments.StoreUnusedArguments(true);

  arguments.AddBooleanArgument(
    "--help", &displayHelp, "Provide a listing of command line options.");
  arguments.AddBooleanArgument("/?", &displayHelp, "Provide a listing of command line options.");

  bool hideMap = false;
  arguments.AddBooleanArgument("--hide-map", &hideMap, "Hide the map on the floor.");

  arguments.AddArgument("--background-color", argT::MULTI_ARGUMENT, &this->BackgroundColor,
    "R G B 0 to 255 values for the background color");

  arguments.AddArgument("--views", argT::EQUAL_ARGUMENT, &(this->ViewsDirectory),
    "(optional) Where to find the views.");

  bool generateLOD = false;
  arguments.AddBooleanArgument("--generate-lod", &generateLOD,
    "Process the data and generate a LOD version for any large files.");

  this->Desktop = false;
  arguments.AddBooleanArgument("--desktop", &this->Desktop, "Render to the desktop instead of VR.");

  this->UseOpenXR = false;
  arguments.AddBooleanArgument("--openxr", &this->UseOpenXR, "Use OpenXR instead of OpenVR.");

  std::string lodView;
  arguments.AddArgument("--generate-lod-for-view", argT::EQUAL_ARGUMENT, &lodView,
    "Generate LOD files for just the named view.");

  this->Client->AddArguments(arguments);

  arguments.AddArgument("--volume", argT::EQUAL_ARGUMENT, &(this->Volume),
    "(default 1.0) Volume of narration and video playback.");

  if (!arguments.Parse())
  {
    cerr << "Problem parsing arguments" << endl;
    return;
  }

  if (displayHelp)
  {
    cerr << "Usage" << endl << endl << "  mapview [options]" << endl << endl << "Options" << endl;
    cerr << arguments.GetHelp();
    return;
  }

  // generate LODs if requested
  if (generateLOD || lodView.length())
  {
    this->State = GeneratingLOD;
    if (!this->Initialize(argc, argv))
    {
      return;
    }
    this->InitializeViews();
    this->GenerateLODs(lodView);
    return;
  }

  this->AudioHandler = new mvAudioHandler();

  // set audio volume
  if (this->Volume >= 0.0)
  {
    mvAudioHandler::SetVolumeLevel(this->Volume);
  }

  this->DisplayMap = !hideMap;

  if (this->Desktop)
  {
    this->Camera = vtkOpenGLCamera::New();
    this->Camera->SetViewAngle(90);
    this->Renderer = vtkOpenGLRenderer::New();
    this->Renderer->SetNearClippingPlaneTolerance(0.0001);
    this->Interactor = vtkRenderWindowInteractor::New();
    this->Style = vtkInteractorStyleJoystickCamera::New();
    this->RenderWindow = static_cast<vtkOpenGLRenderWindow*>(vtkRenderWindow::New());
    this->RenderWindow->SetSize(1024, 800);
  }
  else if (this->UseOpenXR)
  {
    this->Renderer = vtkOpenXRRenderer::New();
    this->Camera = vtkOpenGLCamera::SafeDownCast(this->Renderer->MakeCamera());
    this->Interactor = vtkOpenXRRenderWindowInteractor::New();
    this->Style = vtkOpenXRInteractorStyle::New();
    this->RenderWindow = vtkOpenXRRenderWindow::New();
  }
  else
  {
    this->Renderer = vtkOpenVRRenderer::New();
    this->Camera = vtkOpenGLCamera::SafeDownCast(this->Renderer->MakeCamera());
    this->Interactor = vtkOpenVRRenderWindowInteractor::New();
    this->Style = vtkOpenVRInteractorStyle::New();
    this->RenderWindow = vtkOpenVRRenderWindow::New();
  }

  if (!this->Initialize(argc, argv))
  {
    return;
  }
  if (this->DisplayMap)
  {
    this->LoadMap();
  }
  this->InitializeViews();
  this->Start();
}

mvLobby::~mvLobby()
{
  if (this->EventCommand)
  {
    this->EventCommand->Delete();
  }
  for (auto view : this->Views)
  {
    delete view;
  }
  delete this->AudioHandler;
  this->Client->Delete();

  if (this->Style)
  {
    this->Style->Delete();
  }
  if (this->Interactor)
  {
    this->Interactor->Delete();
  }
  if (this->Camera)
  {
    this->Camera->Delete();
  }
  if (this->Renderer)
  {
    this->Renderer->Delete();
  }
  if (this->RenderWindow)
  {
    this->RenderWindow->Delete();
  }
  delete this->PhysicalPose;
}

vtkOpenGLRenderer* mvLobby::GetRenderer()
{
  return this->Renderer;
}

void mvLobby::AsynchronousLoadView()
{
  // load a view's data
  this->Views[this->CurrentView]->LoadMinimumData(this->AsynchronousLoadViewAbort);
  this->AsynchronousLoadCompleted = true;
}

void mvLobby::RenderLock()
{
  this->RenderMutex.lock();
}

void mvLobby::RenderUnlock()
{
  this->RenderMutex.unlock();
}

void mvLobby::CancelLoadView()
{
  if (this->State != LoadingView)
  {
    return;
  }

  this->State = CancelAsynchronousLoad;
  this->AsynchronousLoadViewAbort = true;
}

void mvLobby::Start()
{
  if (this->LobbyAudio.size())
  {
    PlaySound(
      this->LobbyAudio.c_str(), nullptr, SND_ASYNC | SND_FILENAME | SND_NODEFAULT | SND_LOOP);
  }

  auto* orwi = vtkOpenVRRenderWindowInteractor::SafeDownCast(this->Interactor);
  if (orwi)
  {
    std::string mname = this->ProgramPath.size() ? this->ProgramPath + "/mineview_actions.json"
                                                 : "./mineview_actions.json";
    orwi->SetActionManifestFileName(mname);
  }

  auto* oxrwi = vtkOpenXRRenderWindowInteractor::SafeDownCast(this->Interactor);
  if (oxrwi)
  {
    std::string mname = this->ProgramPath.size() ? this->ProgramPath + "/mineview_openxr_actions.json"
                                                 : "./mineview_openxr_actions.json";
    oxrwi->SetActionManifestFileName(mname);
  }

  this->RenderWindow->Initialize();
  this->Interactor->Initialize();
  if (this->Desktop)
  {
    while (!this->Interactor->GetDone())
    {
      this->Interactor->ProcessEvents();
      this->Interactor->Render();
    }
  }
  else
  {
    this->Interactor->Start();
  }
}

int mvLobby::GetViewNumberFromEvent(vtkEventDataDevice3D* edd)
{
  // find intersection, from
  // http://mathworld.wolfram.com/Circle-LineIntersection.html
  const double* worldPos = edd->GetWorldPosition();
  const double* worldDir = edd->GetWorldDirection();
  double x1 = worldPos[0];
  double x2 = worldPos[0] + worldDir[0];
  double y1 = worldPos[1];
  double y2 = worldPos[1] + worldDir[1];
  double dx = x2 - x1;
  double dy = y2 - y1;
  double D = x1 * y2 - x2 * y1;
  double sdy = dy < 0 ? -1.0 : 1.0;
  double drdr = dx * dx + dy * dy;
  double rr = this->PosterRadius * this->PosterRadius;
  double delta = rr * drdr - D;
  // do we not have an intersection?
  if (delta < 0)
  {
    return -1;
  }

  // find the intersection points
  double rx1 = (D * dy + sdy * dx * sqrt(rr * drdr - D * D)) / drdr;
  double ry1 = (-D * dx + fabs(dy) * sqrt(rr * drdr - D * D)) / drdr;
  double rx2 = (D * dy - sdy * dx * sqrt(rr * drdr - D * D)) / drdr;
  double ry2 = (-D * dx - fabs(dy) * sqrt(rr * drdr - D * D)) / drdr;
  // now solve for 3D points
  double r1[3];
  r1[0] = rx1;
  r1[1] = ry1;
  double t1 = 0.0;
  if (worldDir[0])
  {
    t1 = (r1[0] - worldPos[0]) / worldDir[0];
  }
  else
  {
    if (worldDir[1])
    {
      t1 = (r1[1] - worldPos[1]) / worldDir[1];
    }
    else
    {
      return -1;
    }
  }
  r1[2] = worldPos[2] + t1 * worldDir[2];
  double r2[3];
  double t2 = 0.0;
  r2[0] = rx2;
  r2[1] = ry2;
  if (worldDir[0])
  {
    t2 = (r2[0] - worldPos[0]) / worldDir[0];
  }
  else
  {
    t2 = (r2[1] - worldPos[1]) / worldDir[1];
  }
  r2[2] = worldPos[2] + t2 * worldDir[2];

  // now find the view number
  int numViews = static_cast<int>(this->Views.size());
  double arcstep = this->ArcContentSize + this->ArcGap;
  int viewNum = -1;

  auto getView = [&](double t1, double r1[3]) {
    if (r1[2] > 0.5 && r1[2] < (0.5 + this->PosterHeight))
    {
      double angle = vtkMath::DegreesFromRadians(
        atan2(r1[1] - this->PosterCenter[1], r1[0] - this->PosterCenter[0]));
      if (angle < -90)
      {
        angle = 360 + angle;
      }
      double count = (this->ArcStart - angle) / arcstep;
      if (count - floor(count) < this->ArcContentSize / arcstep)
      {
        if (static_cast<int>(floor(count)) < this->Views.size())
        {
          return static_cast<int>(floor(count));
        }
      }
    }
    return -1;
  };

  // try closest if that is t1
  if (t1 > 0 && (t1 < t2 || t2 < 0))
  {
    viewNum = getView(t1, r1);
  }
  // otherwise try t2
  if (viewNum == -1 && t2 > 0)
  {
    viewNum = getView(t2, r2);
  }
  // if still no match try t1 even if not the closest
  if (viewNum == -1 && t1 > 0)
  {
    viewNum = getView(t1, r1);
  }

  return viewNum;
}

void mvLobby::ServerTourStopChanged(
  int viewIndex, double updateTranslation[3], double updateDirection[3])
{
  if (this->CurrentView >= 0)
  {
    this->Views[this->CurrentView]->GoToTourStop(
      viewIndex, false, true, updateTranslation, updateDirection);
  }
}

void mvLobby::ServerViewChanged(std::string const& viewName)
{
  if (viewName == "Lobby")
  {
    // View "Lobby" is the lobby for collaborators.
    if (this->State != InLobby)
    {
      this->ReturnFromView(true);
    }
  }
  else
  {
    int viewIndex = 0;
    for (auto view : this->Views)
    {
      if (view->GetName() == viewName)
      {
        // if we are already in that view reset to the first position
        if (this->State == InView && this->CurrentView == viewIndex)
        {
          this->Views[this->CurrentView]->GoToTourStop(0, false, false);
        }
        else
        {
          // if we are in another view, exit that one first
          if (this->State != InLobby)
          {
            this->ReturnFromView(true);
          }
          this->LoadView(viewIndex, true);
        }
        break;
      }
      viewIndex++;
    }
  }
}

void mvLobby::EventCallback(
  vtkObject* caller, unsigned long eventID, void* clientdata, void* calldata)
{
  mvLobby* self = static_cast<mvLobby*>(clientdata);

  if (eventID == vtkCommand::StartEvent)
  {
    self->RenderMutex.lock();
  }

  if (eventID == vtkCommand::EndEvent)
  {
    self->RenderMutex.unlock();
  }

  if (eventID == vtkCommand::RenderEvent)
  {
    if (self->AsynchronousLoadCompleted)
    {
      self->FinishLoadView();
    }
    self->Client->Render();
  }

  // we consume all select events and menu events and viewer movement
  if (eventID == vtkCommand::Select3DEvent || eventID == vtkCommand::Menu3DEvent ||
    eventID == vtkCommand::NextPose3DEvent)
  {
    self->EventCommand->AbortFlagOn();
    self->Style->FindPokedRenderer(0, 0);
  }

  // let the view process the button/other event if loaded
  if (self->State == InView)
  {
    if (self->Views[self->CurrentView]->EventCallback(eventID, calldata))
    {
      self->EventCommand->AbortFlagOn();
    }
    return;
  }

  if (eventID == vtkCommand::LeftButtonPressEvent && self->Desktop)
  {
    if (self->State == InLobby)
    {
      // build edd from mouse click
      vtkNew<vtkEventDataDevice3D> edd;
      int* mouse = self->Interactor->GetEventPosition();
      self->Renderer->SetDisplayPoint(mouse[0], mouse[1], -1.0);
      self->Renderer->DisplayToWorld();
      auto worldCoords = self->Renderer->GetWorldPoint();
      if (worldCoords[3] == 0.0)
      {
        return;
      }
      double pos[3];
      for (int i = 0; i < 3; i++)
      {
        pos[i] = worldCoords[i] / worldCoords[3];
      }
      edd->SetWorldPosition(pos);

      self->Renderer->SetDisplayPoint(mouse[0], mouse[1], 0.0);
      self->Renderer->DisplayToWorld();
      worldCoords = self->Renderer->GetWorldPoint();
      if (worldCoords[3] == 0.0)
      {
        return;
      }
      double dir[3];
      for (int i = 0; i < 3; i++)
      {
        dir[i] = worldCoords[i] / worldCoords[3] - pos[i];
      }
      vtkMath::Normalize(dir);
      edd->SetWorldDirection(dir);
      int viewNum = self->GetViewNumberFromEvent(edd);
      if (viewNum >= 0 && self->CurrentView == -1)
      {
        self->EventCommand->AbortFlagOn();
        if (self->Views[viewNum]->GetActive())
        {
          self->EventCommand->AbortFlagOn();
          self->LoadView(viewNum);
        }
      }
      return;
    }
  }

  if (eventID == vtkCommand::CharEvent && self->Desktop)
  {
    auto kc = self->Interactor->GetKeyCode();
    switch (kc)
    {
      case 0x1B: // escape
      case 'Q':
      case 'q':
        if (self->State == LoadingView)
        {
          self->CancelLoadView();
          return;
        }
        if (self->State == InLobby)
        {
          self->Interactor->TerminateApp();
        }
        self->EventCommand->AbortFlagOn();
        return;
    }
  }

  if (eventID == vtkCommand::RenderEvent && self->State == LoadingView)
  {
    self->Views[self->CurrentView]->EventCallback(eventID, calldata);
  }
  else if (eventID == vtkCommand::Menu3DEvent)
  {
    vtkEventData* edata = static_cast<vtkEventData*>(calldata);
    vtkEventDataDevice3D* edd = edata->GetAsEventDataDevice3D();
    if (!edd)
    {
      return;
    }

    if (edd->GetAction() == vtkEventDataAction::Release)
    {
      if (self->State == LoadingView)
      {
        self->CancelLoadView();
        return;
      }
      if (self->State == InLobby)
      {
        self->Interactor->TerminateApp();
      }
    }
    return;
  }
  else if (eventID == vtkCommand::Select3DEvent)
  {
    vtkEventData* edata = static_cast<vtkEventData*>(calldata);
    vtkEventDataDevice3D* edd = edata->GetAsEventDataDevice3D();
    if (!edd)
    {
      return;
    }

    if (edd->GetAction() == vtkEventDataAction::Press)
    {
      if (self->State == InLobby)
      {
        static_cast<vtkVRInteractorStyle*>(self->Style)->ShowRay(edd->GetDevice());
        std::vector<vtkVRCollaborationClient::Argument> args;
        args.resize(1);
        args[0].SetInt32(static_cast<int>(edd->GetDevice()));
        self->Client->SendAMessage("SR", args);
        auto rw = vtkVRRenderWindow::SafeDownCast(self->RenderWindow);
        if (rw)
        {
          vtkVRModel* mod = rw->GetTrackedDeviceModel(edd->GetDevice());
          if (mod)
          {
            // Set length to its max if interactive picking is off
            mod->SetRayLength(self->Camera->GetClippingRange()[1]);
          }
        }
      }
    }

    if (edd->GetAction() == vtkEventDataAction::Release)
    {
      static_cast<vtkVRInteractorStyle*>(self->Style)->HideRay(edd->GetDevice());
      std::vector<vtkVRCollaborationClient::Argument> args;
      args.resize(1);
      args[0].SetInt32(static_cast<int>(edd->GetDevice()));
      self->Client->SendAMessage("HR", args);
      if (self->State == InLobby)
      {
        int viewNum = self->GetViewNumberFromEvent(edd);
        if (viewNum >= 0 && self->CurrentView == -1)
        {
          if (self->Views[viewNum]->GetActive())
          {
            self->LoadView(viewNum);
          }
        }
        return;
      }
    }

    if (edd->GetInput() == vtkEventDataDeviceInput::TrackPad &&
      edd->GetAction() == vtkEventDataAction::Release)
    {
      static_cast<vtkVRInteractorStyle*>(self->Style)->EndDolly3D(edd);
    }
    return;
  }
}

void mvLobby::GenerateLODs(std::string const& viewName)
{
  for (auto view : this->Views)
  {
    if (viewName.length() == 0 ||
      viewName == vtksys::SystemTools::GetFilenameName(view->GetRootDirectory()))
    {
      cerr << "Generating LODs for "
           << vtksys::SystemTools::GetFilenameName(view->GetRootDirectory()) << "\n";
      view->GenerateLODs();
    }
  }
}

std::string ws2s(const std::wstring& wstr)
{
  using convert_typeX = std::codecvt_utf8<wchar_t>;
  std::wstring_convert<convert_typeX, wchar_t> converterX;

  return converterX.to_bytes(wstr);
}

std::string GetViewsDirectoryDialog()
{
  // CoCreate the File Open Dialog object.
  std::string name;
  IFileDialog* pfd = NULL;
  HRESULT hr =
    CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd));
  if (SUCCEEDED(hr))
  {
    // Create an event handling object, and hook it up to the dialog.
    // IFileDialogEvents* pfde = NULL;
    // hr = CDialogEventHandler_CreateInstance(IID_PPV_ARGS(&pfde));
    // if (SUCCEEDED(hr))
    {
      // Hook up the event handler.
      // DWORD dwCookie;
      // hr = pfd->Advise(pfde, &dwCookie);
      // if (SUCCEEDED(hr))
      {
        // Set the options on the dialog.
        DWORD dwFlags;

        // Before setting, always get the options first in order
        // not to override existing options.
        hr = pfd->GetOptions(&dwFlags);
        if (SUCCEEDED(hr))
        {
          // In this case, get shell items only for file system items.
          hr = pfd->SetOptions(dwFlags | FOS_PICKFOLDERS);
          if (SUCCEEDED(hr))
          {
            pfd->SetTitle(L"Select Views Directory");
            // Show the dialog
            hr = pfd->Show(NULL);
            if (SUCCEEDED(hr))
            {
              // Obtain the result once the user clicks
              // the 'Open' button.
              // The result is an IShellItem object.
              IShellItem* psiResult;
              hr = pfd->GetResult(&psiResult);
              if (SUCCEEDED(hr))
              {
                // We are just going to print out the
                // name of the file for sample sake.
                PWSTR pszFilePath = NULL;
                hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
                if (SUCCEEDED(hr))
                {
                  std::wstring wname = pszFilePath;
                  name = ws2s(wname);
                  CoTaskMemFree(pszFilePath);
                }
                psiResult->Release();
              }
            }
          }
        }
        // Unhook the event handler.
        // pfd->Unadvise(dwCookie);
      }
      // pfde->Release();
    }
    pfd->Release();
  }
  return name;
}

bool mvLobby::FindViewsDirectory(char* argv0)
{
  // if specified on the command line, and it exists use that
  if (this->ViewsDirectory.length() &&
    vtksys::SystemTools::FileExists(this->ViewsDirectory.c_str(), false))
  {
    return true;
  }

  // if found relative to the program, use that
  {
    // handle command line args, get the exe dir
    std::string errorMsg;
    bool found = vtksys::SystemTools::FindProgramPath(argv0, this->ViewsDirectory, errorMsg);
    if (found)
    {
      this->ViewsDirectory = vtksys::SystemTools::CollapseFullPath(this->ViewsDirectory);
      this->ViewsDirectory = vtksys::SystemTools::GetProgramPath(this->ViewsDirectory);
      this->ProgramPath = this->ViewsDirectory;
      std::string testDir = this->ViewsDirectory + "/Views";
      if (vtksys::SystemTools::FileExists(testDir.c_str(), false))
      {
        this->ViewsDirectory = testDir;
        return true;
      }

      // try ../
      this->ViewsDirectory += "/..";
      this->ViewsDirectory = vtksys::SystemTools::CollapseFullPath(this->ViewsDirectory);
      testDir = this->ViewsDirectory + "/Views";
      if (vtksys::SystemTools::FileExists(testDir.c_str(), false))
      {
        this->ViewsDirectory = testDir;
        return true;
      }
    }
  }

  // look in Documents
  {
    size_t rlen = 0;
    char uprofile[1024];
    if (getenv_s(&rlen, uprofile, 1000, "USERPROFILE") && rlen)
    {
      std::string testDir = vtksys::SystemTools::CollapseFullPath(uprofile);
      testDir += "/Documents/Views";
      if (vtksys::SystemTools::FileExists(testDir.c_str(), false))
      {
        this->ViewsDirectory = testDir;
        return true;
      }
    }
  }

  {
    std::string testDir = GetViewsDirectoryDialog();
    if (testDir.length() && vtksys::SystemTools::FileExists(testDir.c_str(), false))
    {
      this->ViewsDirectory = testDir;
      return true;
    }
  }

  vtkOutputWindow::GetInstance()->PromptUserOn();
  vtkGenericWarningMacro("Unable to find Views subdirectory");
  return false;
}

bool mvLobby::Initialize(int argc, char* argv[])
{
  if (!this->FindViewsDirectory(argv[0]))
  {
    return false;
  }

  // look for lobby audio file
  this->LobbyAudio = this->ViewsDirectory + "/Lobby.wav";
  if (!vtksys::SystemTools::FileExists(this->LobbyAudio.c_str(), true))
  {
    this->LobbyAudio = "";
  }

  this->Renderer->SetActiveCamera(this->Camera);
  this->RenderWindow->AddRenderer(this->Renderer);
  this->Renderer->SetClippingRangeExpansion(0.01);
  if (this->BackgroundColor.size() == 3)
  {
    this->Renderer->SetBackground(this->BackgroundColor[0] / 255.0,
      this->BackgroundColor[1] / 255.0, this->BackgroundColor[2] / 255.0);
  }
  this->Interactor->SetRenderWindow(this->RenderWindow);
  this->Interactor->SetInteractorStyle(this->Style);
  this->Renderer->RemoveCuller(this->Renderer->GetCullers()->GetLastItem());

  // initialize the camera
  this->Camera->SetViewUp(0, 0, 1);
  this->Camera->SetPosition(0, -5, 2);
  this->Camera->SetFocalPoint(0, 0, 2);

  {
    vtkLight* light = vtkLight::New();
    light->SetPosition(0.0, 1.0, 0.0);
    light->SetIntensity(1.0);
    light->SetLightTypeToSceneLight();
    this->Renderer->AddLight(light);
    light->Delete();
  }
  {
    vtkLight* light = vtkLight::New();
    light->SetPosition(0.8, -0.2, 0.0);
    light->SetIntensity(0.8);
    light->SetLightTypeToSceneLight();
    this->Renderer->AddLight(light);
    light->Delete();
  }
  {
    vtkLight* light = vtkLight::New();
    light->SetPosition(-0.3, -0.2, 0.7);
    light->SetIntensity(0.6);
    light->SetLightTypeToSceneLight();
    this->Renderer->AddLight(light);
    light->Delete();
  }
  {
    vtkLight* light = vtkLight::New();
    light->SetPosition(-0.3, -0.2, -0.7);
    light->SetIntensity(0.4);
    light->SetLightTypeToSceneLight();
    this->Renderer->AddLight(light);
    light->Delete();
  }

  this->RenderWindow->SetMultiSamples(8);

  this->Client->SetLobby(this);
  this->Client->Initialize(this->Renderer);

  this->EventCommand = vtkCallbackCommand::New();
  this->EventCommand->SetClientData(this);
  this->EventCommand->SetCallback(mvLobby::EventCallback);

  this->Renderer->AddObserver(vtkCommand::StartEvent, this->EventCommand, 2.0);
  this->Renderer->AddObserver(vtkCommand::EndEvent, this->EventCommand, 2.0);

  this->Interactor->AddObserver(vtkCommand::RenderEvent, this->EventCommand, 2.0);

  this->SetPhysicalViewUp(0, 0, 1);
  this->SetPhysicalViewDirection(0, 1, 0);

  auto rw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (rw)
  {
    this->Interactor->AddObserver(vtkCommand::Select3DEvent, this->EventCommand, 2.0);
    this->Interactor->AddObserver(vtkCommand::Menu3DEvent, this->EventCommand, 2.0);
    this->Interactor->AddObserver(vtkCommand::ViewerMovement3DEvent, this->EventCommand, 2.0);
    this->Interactor->AddObserver(vtkCommand::NextPose3DEvent, this->EventCommand, 2.0);
  }
  else
  {
    this->Interactor->AddObserver(vtkCommand::CharEvent, this->EventCommand, 2.0);
    this->Interactor->AddObserver(vtkCommand::LeftButtonPressEvent, this->EventCommand, 2.0);
  }

  this->Style->SetCurrentRenderer(this->Renderer);
  // this->Style->ToggleDrawControls();

  this->State = InLobby;

  return true;
}

double mvLobby::GetPhysicalScale()
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    return vrrw->GetPhysicalScale();
  }

  return this->PhysicalPose->Distance;
}

void mvLobby::SetPhysicalScale(double val)
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    vrrw->SetPhysicalScale(val);
  }
  this->PhysicalPose->Distance = val;
}

double* mvLobby::GetPhysicalTranslation()
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    return vrrw->GetPhysicalTranslation();
  }

  return this->PhysicalPose->Translation;
}

void mvLobby::SetPhysicalTranslation(double v1, double v2, double v3)
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    vrrw->SetPhysicalTranslation(v1, v2, v3);
  }

  this->PhysicalPose->Translation[0] = v1;
  this->PhysicalPose->Translation[1] = v2;
  this->PhysicalPose->Translation[2] = v3;
}

double* mvLobby::GetPhysicalViewDirection()
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    return vrrw->GetPhysicalViewDirection();
  }

  return this->PhysicalPose->PhysicalViewDirection;
}

void mvLobby::SetPhysicalViewDirection(double v1, double v2, double v3)
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    vrrw->SetPhysicalViewDirection(v1, v2, v3);
  }

  this->PhysicalPose->PhysicalViewDirection[0] = v1;
  this->PhysicalPose->PhysicalViewDirection[1] = v2;
  this->PhysicalPose->PhysicalViewDirection[2] = v3;
}

double* mvLobby::GetPhysicalViewUp()
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    return vrrw->GetPhysicalViewUp();
  }

  return this->PhysicalPose->PhysicalViewUp;
}

void mvLobby::SetPhysicalViewUp(double v1, double v2, double v3)
{
  auto vrrw = vtkVRRenderWindow::SafeDownCast(this->RenderWindow);
  if (vrrw)
  {
    vrrw->SetPhysicalViewUp(v1, v2, v3);
  }

  this->PhysicalPose->PhysicalViewUp[0] = v1;
  this->PhysicalPose->PhysicalViewUp[1] = v2;
  this->PhysicalPose->PhysicalViewUp[2] = v3;
}

void mvLobby::LoadMap()
{
  // create flat earth
  std::string fileName = this->ViewsDirectory;
  fileName += "\\World4096.jpg";

  // check if the map file is present, if not take the first jpeg found
  if (!vtksys::SystemTools::FileExists(fileName.c_str(), true))
  {
    vtkNew<vtkGlobFileNames> glob;
    glob->SetDirectory(this->ViewsDirectory.c_str());
    glob->RecurseOff();
    glob->AddFileNames("*.jpg");
    int numjpg = glob->GetNumberOfFileNames();
    if (numjpg > 0)
    {
      fileName = glob->GetNthFileName(0);
    }
  }

  // if no file then sip the map
  if (!vtksys::SystemTools::FileExists(fileName.c_str(), true))
  {
    this->DisplayMap = false;
    return;
  }

  vtkNew<vtkJPEGReader> mapReader;
  mapReader->SetFileName(fileName.c_str());
  vtkNew<vtkTexture> mapTexture;
  mapTexture->SetInputConnection(mapReader->GetOutputPort());
  mapTexture->InterpolateOn();
  mapTexture->MipmapOn();

  vtkNew<vtkPlaneSource> mapSource;
  mapSource->SetOrigin(-2.0, -1.0, 0.0);
  mapSource->SetPoint1(2.0, -1.0, 0.0);
  mapSource->SetPoint2(-2.0, 1.0, 0.0);
  vtkNew<vtkPolyDataMapper> mapMapper;
  mapMapper->SetInputConnection(mapSource->GetOutputPort());
  this->MapActor->GetProperty()->SetAmbient(1.0);
  this->MapActor->GetProperty()->SetDiffuse(0.0);
  this->MapActor->SetMapper(mapMapper);
  this->MapActor->SetTexture(mapTexture);
  this->Renderer->AddActor(this->MapActor);
}

namespace
{
bool sortViews(mvView* a, mvView* b)
{
  double angleA = atan2(a->GetLatitude(), a->GetLongitude());
  double angleB = atan2(b->GetLatitude(), b->GetLongitude());
  if (angleA < -M_PI / 2.0)
  {
    angleA = 2.0 * M_PI + angleA;
  }
  if (angleB < -M_PI / 2.0)
  {
    angleB = 2.0 * M_PI + angleB;
  }
  return (angleB < angleA);
}
}

void mvLobby::InitializeViews()
{
  // look in subdirs for Views and map them
  vtkNew<vtkGlobFileNames> glob;
  glob->SetDirectory(this->ViewsDirectory.c_str());
  glob->RecurseOn();
  glob->AddFileNames("*.mvx");

  // each view subtends ArcContentSize degree arc with a ArcGap degree gap
  int numViews = glob->GetNumberOfFileNames();
  for (int i = 0; i < numViews; ++i)
  {
    // extract the data
    mvView* view = new mvView();
    view->Initialize(glob->GetNthFileName(i), this);
    this->Views.push_back(view);
  }
  std::sort(this->Views.begin(), this->Views.end(), sortViews);

  // use the first view's poster aspect ratio as the standard
  this->Views[0]->GetPosterActor()->GetTexture()->Update();
  int* dims = this->Views[0]->GetPosterActor()->GetTexture()->GetInput()->GetDimensions();

  double targetWidth = this->PosterHeight * dims[0] / dims[1];
  this->ArcContentSize = 360.0 * targetWidth / (2.0 * M_PI * this->PosterRadius);
  double totalArc = this->ArcContentSize * numViews + this->ArcGap * (numViews - 1);

  // shrink poster height if needed
  while (totalArc + this->ArcGap > 360.0)
  {
    this->PosterHeight *= 0.9;
    targetWidth = this->PosterHeight * dims[0] / dims[1];
    this->ArcContentSize = 360.0 * targetWidth / (2.0 * M_PI * this->PosterRadius);
    totalArc = this->ArcContentSize * numViews + this->ArcGap * (numViews - 1);
  }

  this->ArcStart = totalArc / 2.0 + 90.0;
  double arcstep = this->ArcContentSize + this->ArcGap;
  double acsRad = vtkMath::RadiansFromDegrees(this->ArcContentSize);
  int i = 0;
  for (auto view : this->Views)
  {
    double pos = vtkMath::RadiansFromDegrees(this->ArcStart - i * arcstep);
    view->SetPosition(this->PosterRadius * cos(pos) + this->PosterCenter[0],
      this->PosterRadius * sin(pos) + this->PosterCenter[1],
      this->PosterRadius * cos(pos - acsRad) + this->PosterCenter[0],
      this->PosterRadius * sin(pos - acsRad) + this->PosterCenter[1], this->PosterHeight);
    ++i;
  }

  this->Renderer->ResetCameraClippingRange();
}

void mvLobby::LoadView(size_t viewNum, bool fromCollab)
{
  // stop any audio
  PlaySound(nullptr, nullptr, SND_ASYNC);

  this->CurrentView = static_cast<int>(viewNum);
  if (this->State == InLobby && !fromCollab)
  {
    std::vector<vtkVRCollaborationClient::Argument> args;
    args.resize(1);
    args[0].SetString(this->Views[this->CurrentView]->GetName());
    this->Client->SendAMessage("V", args);
  }

  this->State = LoadingView;
  this->Views[this->CurrentView]->LoadMainThread();
  this->AsynchronousLoadViewAbort = false;
  this->AsynchronousLoadCompleted = false;
  this->Threader = new std::thread(&mvLobby::AsynchronousLoadView, this);
}

void mvLobby::FinishLoadView()
{
  this->Threader->join();
  delete this->Threader;
  this->Threader = nullptr;
  // reset the flag
  this->AsynchronousLoadCompleted = false;

  if (this->State != LoadingView)
  {
    this->ReturnFromView();
    return;
  }

  this->State = InView;

  // turn off lobby actors
  this->MapActor->VisibilityOff();
  for (auto v : this->Views)
  {
    v->LobbyOff();
  }

  this->Views[this->CurrentView]->FinishLoadView();
  this->Views[this->CurrentView]->GoToFirstTourStop();
}

void mvLobby::ReturnFromView(bool fromCollab)
{
  if (this->State == InView && !fromCollab)
  {
    std::vector<vtkVRCollaborationClient::Argument> args;
    args.resize(1);
    args[0].SetString(std::string("Lobby"));
    this->Client->SendAMessage("V", args);
  }

  this->Views[this->CurrentView]->ReturnFromView();

  // turn back on lobby actors
  if (this->DisplayMap)
  {
    this->MapActor->VisibilityOn();
  }
  for (auto v : this->Views)
  {
    v->LobbyOn();
  }

  this->Camera->SetViewUp(0, 0, 1);
  this->Camera->SetPosition(0, -5, 2);
  this->Camera->SetFocalPoint(0, 0, 2);

  double trans[3] = { 0.0, 0.0, 0.0 };
  this->SetPhysicalScale(1.0);
  this->SetPhysicalTranslation(trans);
  this->SetPhysicalViewUp(0, 0, 1);
  this->SetPhysicalViewDirection(0, 1, 0);

  this->Renderer->ResetCameraClippingRange();

  if (this->LobbyAudio.size())
  {
    PlaySound(
      this->LobbyAudio.c_str(), nullptr, SND_ASYNC | SND_FILENAME | SND_NODEFAULT | SND_LOOP);
  }

  this->State = InLobby;
  this->CurrentView = -1;
}
