/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "mvLobbyClient.h"

#include "mvLobby.h"
#include "mvView.h"
#include "vtkCallbackCommand.h"
#include "vtkEventData.h"
#include "vtkObjectFactory.h"
#include "vtkVRModel.h"
#include "vtkVRRenderWindow.h"
#include "vtkVRRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkTimerLog.h"

#include <sstream>

// simple macro to package up arguments to call Log()
#define mvLog(verbosity, x)                                                                        \
  std::ostringstream ss;                                                                           \
  ss << x;                                                                                         \
  this->Log(verbosity, ss.str());

vtkStandardNewMacro(mvLobbyClient);

mvLobbyClient::mvLobbyClient()
{
  this->ScaleCallback = [this]() {
    return this->Lobby->GetPhysicalScale();
  };
}

void mvLobbyClient::HandleBroadcastMessage(
  std::string const &otherID,
  std::string const &type)
{
  if (type == "V")
  {
    // change view.
    std::vector<Argument> args = this->GetMessageArguments();
    // Set avatar's name, displayed above head.
    std::string viewName;
    if (args.size() != 1 || !args[0].GetString(viewName))
    {
      mvLog(vtkLogger::VERBOSITY_ERROR, "Incorrect arguments for V (view) collaboration message" << std::endl);
      return;
    }

    std::cout << "Collab " << otherID << ", View: " <<
        viewName << " " << std::endl;
    // only want to change if it's from someone else.
    if (otherID != this->CollabID)
    {
      this->Lobby->ServerViewChanged(viewName);
    }
  }
  else if (type == "P")
  {
    // change pose.
    std::vector<Argument> args = this->GetMessageArguments();

    int viewIndex = 0;
    std::vector<double> uTrans;
    std::vector<double> uDir;
    if (args.size() != 3 || !args[0].GetInt32(viewIndex) || !args[1].GetDoubleVector(uTrans) ||
      !args[2].GetDoubleVector(uDir))
    {
      mvLog(vtkLogger::VERBOSITY_ERROR, "Incorrect arguments for P (avatar pose) collaboration message" << std::endl);
      return;
    }

    std::cout << "Collab " << otherID << ", Pose: " <<
        viewIndex << " " <<
        uTrans[0] << " " <<
        uTrans[1] << " " <<
        uTrans[2] << " " <<
        std::endl;

    // only want to change if it's from someone else.
    if (otherID != this->CollabID)
    {
      this->Lobby->ServerTourStopChanged(
        viewIndex, uTrans.data(), uDir.data());
    }
  }
  else if (type == "J")
  {
    // Join message, send our list of views.
    std::vector<Argument> args = this->GetMessageArguments();

    std::string extraID;
    if (args.size() != 1 || !args[0].GetString(extraID))
    {
      mvLog(vtkLogger::VERBOSITY_ERROR, "Incorrect arguments for J (join) collaboration message" << std::endl);
      return;
    }

    // if we are idle, don't respond to join messages - send a join when
    // we are not idle anymore.
    if (this->AvatarIdle(this->CollabID))
    {
      return;
    }
    std::cout << "Collab " << otherID << ", Join" << std::endl;
    // send our list of views, even if we just joined.
    std::vector<std::string> viewNames;
    for (auto view: this->Lobby->Views)
    {
      viewNames.push_back(view->GetName());
      // re-activate all views, until view-lists come in.
      view->SetActive(true);
    }
    {
      std::vector<vtkVRCollaborationClient::Argument> args2;
      args2.resize(1);
      args2[0].SetStringVector(viewNames);
      this->SendAMessage("VL", args2);
    }
    if (!this->CollabName.empty())
    {
      std::vector<vtkVRCollaborationClient::Argument> args2;
      args2.resize(1);
      args2[0].SetString(this->CollabName);
      this->SendAMessage("N", args2);
    }
  }
  else if (type == "VL")
  {
    std::vector<Argument> args = this->GetMessageArguments();

    std::vector<std::string> viewNames;
    if (args.size() != 1 || !args[0].GetStringVector(viewNames))
    {
      mvLog(vtkLogger::VERBOSITY_ERROR, "Incorrect arguments for VL (view list) collaboration message" << std::endl);
      return;
    }

    if (otherID != this->CollabID)
    {
      // if a view is not in the list, de-activate
      for(auto view: this->Lobby->Views)
      {
        if (std::find(viewNames.begin(), viewNames.end(), view->GetName()) == viewNames.end())
        {
          std::cout << "  de-activate: " << view->GetName() << std::endl;
          view->SetActive(false);
        }
      }
    }
  }
  else
  {
    this->vtkVRCollaborationClient::HandleBroadcastMessage(otherID, type);
  }
}
