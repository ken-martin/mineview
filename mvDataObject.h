/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <map>
class vtkActor;
class vtkAlgorithmOutput;
class vtkCommand;
class vtkDataSet;
class vtkXMLDataElement;
class vtkXMLDataReader;

class mvDataObject
{
public:
  ~mvDataObject();
  void LoadData(vtkXMLDataElement *topel, std::string const &rootDir);
  void SetFileName(std::string const &val, std::string const &rootDir);
  std::string const &GetFileName() { return this->FileName; }

  void FreeMemory();
  void LoadNextLOD(vtkCommand *cb);
  int GetSmallestLOD();
  int GetNumberOfLODs();

  double GetLoadPriority() { return this->LoadPriority; }
  void SetLoadPriority(double l) { this->LoadPriority = l; }
  double ComputeLoadPriority();

  vtkAlgorithmOutput* GetOutputPort();
  vtkDataSet *GetOutputAsDataSet();

  vtkXMLDataReader *GetLODReader(int lod);

  int DataObjectID = -1;
  int DataObjectType = -1;
  int LastLODLoaded = 0;

  std::map<int,std::string> LODs;
  std::map<int,size_t> LODSizes;

private:
  std::map<int, vtkXMLDataReader *> LODReaders;
  vtkXMLDataReader *MakeReader(std::string &fname);
  double LoadPriority;
  std::string FileName;
};
