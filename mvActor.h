/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <map>
class mvDataObject;
class mvTexture;
class vtkActor;
class vtkXMLDataElement;

class mvActor
{
public:
  vtkActor *VTKActor;
  double Opacity;
  int PolyDataID;
  int TextureID;

  int NormalTextureID;
  int AlbedoTextureID;
  int MaterialTextureID;
  int EmissiveTextureID;

  mvActor();
  ~mvActor();
  void LoadData(vtkXMLDataElement *topel);
  void FreeMemory();
  void SetupTextures(std::map<size_t,mvTexture> &textures);
  void ComputeLoadPriority(std::map<size_t,mvTexture> &textures, std::map<size_t, mvDataObject> &);
};
