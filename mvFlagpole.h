/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <vector>
class vtkFlagpoleLabel;
class vtkXMLDataElement;

class mvFlagpole
{
public:
  vtkFlagpoleLabel *FlagpoleLabel = nullptr;
  int NumberOfLocations = 0;
  std::vector<int> Locations;
  mvFlagpole();
  ~mvFlagpole();
  void LoadData(vtkXMLDataElement *topel);
};
