/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "mvLobby.h"

//----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  mvLobby *lobby = new mvLobby(argc, argv);

  delete lobby;
}
