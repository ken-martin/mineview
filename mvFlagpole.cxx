/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "mvFlagpole.h"

#include "vtkFlagpoleLabel.h"
#include "vtkTextProperty.h"
#include "vtkXMLDataElement.h"

mvFlagpole::mvFlagpole()
{
  this->FlagpoleLabel = vtkFlagpoleLabel::New();
}

mvFlagpole::~mvFlagpole()
{
  this->FlagpoleLabel->Delete();
}

void mvFlagpole::LoadData(vtkXMLDataElement *flagel)
{
  this->FlagpoleLabel->SetFlagSize(0.3);
  this->FlagpoleLabel->GetTextProperty()->SetBackgroundColor(0.1,0.1,0.2);
  this->FlagpoleLabel->GetTextProperty()->SetFrameColor(0.1,0.1,0.2);
  this->FlagpoleLabel->GetTextProperty()->SetFrameWidth(5);
  this->FlagpoleLabel->GetTextProperty()->SetBackgroundOpacity(1.0);
  this->FlagpoleLabel->SetInput(flagel->GetAttribute("Label"));
  double dtmp[3] = { 0.0, 0.0, 0.0 };
  flagel->GetVectorAttribute("Position", 3, dtmp);
  this->FlagpoleLabel->SetBasePosition(dtmp[0], dtmp[1], dtmp[2]);
  double height = 1.0;
  flagel->GetScalarAttribute("Height", height);
  this->FlagpoleLabel->SetTopPosition(dtmp[0], dtmp[1], dtmp[2] + height);
  flagel->GetScalarAttribute("NumberOfLocations", this->NumberOfLocations);
  if (this->NumberOfLocations)
  {
    this->Locations.resize(this->NumberOfLocations);
    flagel->GetVectorAttribute("Locations",
      this->NumberOfLocations, &(this->Locations[0]));
  }
}
