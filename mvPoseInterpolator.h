/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkNew.h"
#include <array>
#include "vtkVector.h"
#include "vtkVRCamera.h" // for nested class

class mvLobby;
class vtkCamera;
class vtkCardinalSpline;
class vtkOpenGLRenderWindow;
class vtkVRRenderWindow;
class vtkRenderer;
class vtkTupleInterpolator;

class mvPoseInterpolator
{
public:
  vtkVRCamera::Pose *Start;
  vtkVRCamera::Pose *End;
  vtkVRCamera::Pose *Last;
  vtkVector3d StartViewDirection;
  vtkVector3d StartPhysicalViewDirection;
  bool FlightMode;
  bool AdjustViewDirection;
  double LastPercentage;
  // splines
  vtkNew<vtkTupleInterpolator> Translation;
  // 0-2 translation
  std::array<vtkCardinalSpline*,6> Splines;

  static void ComputeTranslationAndRotation(
    vtkVRCamera::Pose *start,
    vtkVRCamera::Pose *end,
    vtkVRRenderWindow *win,
    double thetaThreshold, // how big a rotation before we rotate, radians
    double *trans, double &rot);

  static void ApplyTranslationAndRotation(
    vtkVRCamera::Pose *start,
    vtkVRCamera::Pose *end,
    vtkVRRenderWindow *win,
    double *trans, double rot);

  mvPoseInterpolator();
  ~mvPoseInterpolator();

  void Fly(
    vtkVRCamera::Pose *start,
    vtkVRCamera::Pose *end,
    vtkRenderer *ren,
    double percent);

  static void Interpolate(
    vtkVRCamera::Pose *prior,
    vtkVRCamera::Pose *next,
    double factor, // 0.0 to 1.0 how far along
    double lastFactor,
    double thetaThreshold, // how big a rotation before we rotate, radians
    mvLobby *lobby);
};
