/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <vector>
#include "vtkSmartPointer.h"
class vtkActor;
class vtkFFMPEGVideoSource;
class vtkOpenGLTexture;
class vtkXMLDataElement;

class mvPhotoSphere
{
public:
  vtkActor *Actor;
  vtkOpenGLTexture *Texture;
  bool IsVideo;
  int Stereo3D;
  vtkSmartPointer<vtkFFMPEGVideoSource> VideoSource;
  std::string TextureFile;
  double Color[3];
  double Position[3];
  double Opacity = 0.6;
  double Front[3];
  double Size = 0.5;
  int NumberOfLocations = 0;
  std::vector<int> Locations;
  mvPhotoSphere() {
    this->Front[0] = 1.0;
    this->Front[1] = 0.0;
    this->Front[2] = 0.0;
    this->Size = 0.0;
    this->Position[0] = 0.0;
    this->Position[1] = 0.0;
    this->Position[2] = 0.0;
    this->Stereo3D = 0;
    this->Actor = nullptr;
    this->Texture = nullptr;
    this->IsVideo = false;
  }
  void LoadData(vtkXMLDataElement *topel, std::string const &rootDir);
};
