/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// This class provides collaboration support for VR
// specific to the mineview application.

#include "vtkVRCollaborationClient.h"

class mvLobby;

class mvLobbyClient : public vtkVRCollaborationClient
{
public:
  static mvLobbyClient* New();
  vtkTypeMacro(mvLobbyClient, vtkVRCollaborationClient);

  void SetLobby(mvLobby *l) {
    this->Lobby = l;
  }

protected:
  mvLobbyClient();
  // ~mvLobbyClient() override;

  void HandleBroadcastMessage(
    std::string const &otherID, std::string const &type) override;

  mvLobby *Lobby;

private:
  mvLobbyClient(const mvLobbyClient&) = delete;
  void operator=(const mvLobbyClient&) = delete;
};
