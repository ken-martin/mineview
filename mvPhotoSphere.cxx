/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "mvPhotoSphere.h"
#include "vtkMath.h"
#include "vtkXMLDataElement.h"
#include "vtksys/SystemTools.hxx"

void mvPhotoSphere::LoadData(
  vtkXMLDataElement *sphereel,
  std::string const &rootDir)
{
  const char *spheretext = sphereel->GetAttribute("TextureFile");
  if (spheretext)
  {
    std::string sname = rootDir + "/" + std::string(spheretext);
    if (vtksys::SystemTools::FileExists(sname.c_str(), true))
    {
      this->TextureFile = sname;
    }
    else
    {
      vtkGenericWarningMacro("Unable to find photosphere texture " << sname);
    }
  }
  else
  {
    vtkGenericWarningMacro("photosphere found with no TextureFile attribute");
  }
  sphereel->GetVectorAttribute("Position", 3, this->Position);
  sphereel->GetVectorAttribute("Color", 3, this->Color);
  sphereel->GetVectorAttribute("Front", 3, this->Front);
  vtkMath::Normalize(this->Front);
  sphereel->GetScalarAttribute("Opacity", this->Opacity);
  sphereel->GetScalarAttribute("Size", this->Size);
  sphereel->GetScalarAttribute("Stereo3D", this->Stereo3D);
  sphereel->GetScalarAttribute("NumberOfLocations", this->NumberOfLocations);
  if (this->NumberOfLocations)
  {
    this->Locations.resize(this->NumberOfLocations);
    sphereel->GetVectorAttribute("Locations",
      this->NumberOfLocations, &(this->Locations[0]));
  }
}
