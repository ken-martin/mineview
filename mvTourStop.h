/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <string>
#include <vector>

class vtkXMLDataElement;

class mvCameraPose
{
public:
  std::vector<int> ActorIDs;
  float PoseNumber;
};

class mvTourStop
{
public:
  std::string MessageBoxText;
  std::string AudioFile;
  std::string ShowPhotoSphere;
  std::string Name;
  bool AutoAdvance = false;
  bool AdvanceToLobby = false;
  mvCameraPose const *CameraPose;
  float AnimationDuration = 2.0;
  float PoseNumber;

  void LoadData(vtkXMLDataElement *topel, std::string const &rootDir,
    std::vector<mvCameraPose>  const &cameraPoses);
};
