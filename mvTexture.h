/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkSmartPointer.h"
class mvDataObject;
class vtkAlgorithmOutput;
class vtkEquirectangularToCubeMapTexture;
class vtkOpenGLRenderWindow;
class vtkOpenGLTexture;
class vtkTextureObject;
class vtkXMLDataElement;

class mvTexture
{
public:
  void LoadData(vtkXMLDataElement *topel);
  void FreeMemory();
  vtkOpenGLTexture *GetOutputTexture();

  void SetImageDataID(int id) { this->ImageDataID = id; }
  int GetImageDataID() { return this->ImageDataID; }

  mvTexture();
  ~mvTexture();

  void CreateInputTextureObject(mvDataObject &dobj, vtkOpenGLRenderWindow *rw);
  void SetInputTextureObject(vtkTextureObject *);
  vtkTextureObject *GetInputTextureObject() {
    return this->TextureObject;
  }

private:
  vtkSmartPointer<vtkTextureObject> TextureObject;
  vtkSmartPointer<vtkOpenGLTexture> OpenGLTexture;
  vtkSmartPointer<vtkEquirectangularToCubeMapTexture> CubeMapTexture;
  int ImageDataID = -1;
};
