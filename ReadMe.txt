Quick start for mineview.

Mineview provides a lobby where users can see mines that are available to be
visited. By clicking on a mine's poster they will then travel to that mine
where they can visit landmarks and eventually return to the lobby.

Controls:  Only one controller is needed and both controllers work the same
way. In the lobby, the trigger can be used to select a poster that the
controller is pointing at. The application menu button (above the trackpad)
will exit the application.

Within a mine the trigger will move to the next saved landmark. The
application menu will exit the mine and return to the lobby. The trackpad
can be used to fly around within the mine.

If you see a green sphere that is a photobubble, if you click on it, it will
take you into the bubble.

---------------------------------------------------------------
Authoring mines (aka views)
---------------------------------------------------------------

Views are exported from ParaView using the Export Locations As View and will
write the results to a folder named pv-view. You should rename this folder
to something that makes sense for that view and then place it in the Views
subdirectory of mineview.

The view contains an index.mvx file and an extra.xml file. You can edit the
extra.xml file to add additional properties to the view such as message
boxes or photospheres.

The first few lines contain values for the viewimage to be used as a poster
(jpg format) and the lat/long location of the mine. Views will be arranged
according to their lat long around the world map.

Message boxes can be provided for specific landmarks in the extra.xml file.
The format is as follows. Note that the sequence &#13;&#10; is used to
represent a newline.

Photospheres can be added as shown in the example file below. Note that if
you do not specify locations then the photosphere is assumed to be always
on.

<View ViewImage="tanami.jpg"
      Longitude="-20.0"
      Latitude="0.0">
  <Flagpoles>
    <Flagpole
        Label="Flagpole Text"
        Position="18765 48420 -40"
        Height="500"
        NumberOfLocations="1"
        Locations="1"
     />
  </Flagpoles>
  <PhotoSpheres>
    <PhotoSphere
        TextureFile="wintersun.jpg"
        Color="0.5 1.0 1.0"
        Size="1.0"
        Position="59956.2 8877.66 2108.12"
        Front="0.7 0.7 0.0"
        NumberOfLocations="2"
        Locations="2 4"
        />
    <PhotoSphere
        TextureFile="IMG_0856.JPG"
        Color="1.0 0.5 0.5"
        Size="1.0"
        Position="59955.2 8877.66 2108.12"
        />
  </PhotoSpheres>
  <CameraPoses>
    <CameraPose PoseNumber="0.5"
        ShowPhotoSphere="DSCN0014-VP9-5mbit-half.mkv"
        AudioFile="Introduction.wav"
        />
    <CameraPose PoseNumber="1"
        MessageBoxText="&#13;&#10;  Location 1 is Awesome &#13;&#10; And full of information. &#13;&#10;"
        AnimationDuration="3.0"
    />
    <CameraPose PoseNumber="1.5"
        ShowPhotoSphere="DSCN0014-VP9-5mbit-half.mkv"
        />
  </CameraPoses>
</View>

Within a view folder you can provide audio files that will get played at
specific locations. They must be named Location#.wav and be a wave file. You
can also have a Lobby.wav file in the mineview directory that will loop
audio for the lobby.

If you wish to disable the System Menu you can do so using the SteamVR
options. From the SteamVR application menu turn off
Settings -> Developer -> Enable VR Dashboard

---------------------------------------------------------------
Converting Videos
---------------------------------------------------------------

Mineview can play vp9 video files as 360 degree spheres as well as top left/
right bottom VR 360 degree video. To convert to vp9 you can use ffmpeg and
some common command lines are provided below. We suggest using mkv as a
container since it allows the largest flexibility in video/audio formats.

// pass the audio through unchanged  -c:a copy
// convert to vp9 with a desired quality of 18 but limited
// to no more than a 8Mb video rate.
// -c:v libvpx-vp9 -crf 20 -b:v 8000k
ffmpeg -i 3d.mp4 -c:a copy -c:v libvpx-vp9 -tile-columns 4 -crf 18 -b:v 8000k dir2.mkv

// same as the above, but at half resolution and 3Mb video rate
// adds -vf "scale=iw*0.5:ih*0.5"
ffmpeg -i 3d.mp4 -c:a copy -vf "scale=iw*0.5:ih*0.5" -c:v libvpx-vp9 -tile-columns 4 -crf 20 -b:v 3000k dir2.mkv

---------------------------------------------------------------
License
---------------------------------------------------------------

All rights reserved (placeholder)

Makes use of unmodified LGPL 2.1 ffmpeg libraries
