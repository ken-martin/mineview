/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "mvTourStop.h"
#include "vtkXMLDataElement.h"
#include "vtksys/SystemTools.hxx"

void mvTourStop::LoadData(
  vtkXMLDataElement *poseel,
  std::string const &rootDir,
  std::vector<mvCameraPose>  const &cameraPoses
  )
{
  int itmp = 0;
  poseel->GetScalarAttribute("AutoAdvance", itmp);
  this->AutoAdvance = (itmp != 0);
  itmp = 0;
  poseel->GetScalarAttribute("AdvanceToLobby", itmp);
  this->AdvanceToLobby = (itmp != 0);

  poseel->GetScalarAttribute("AnimationDuration", this->AnimationDuration);
  const char *mbtext = poseel->GetAttribute("MessageBoxText");
  if (mbtext)
  {
    this->MessageBoxText = mbtext;
  }
  const char *showPhotoSphere = poseel->GetAttribute("ShowPhotoSphere");
  if (showPhotoSphere)
  {
    std::string sname = rootDir + "/" + std::string(showPhotoSphere);
    if (vtksys::SystemTools::FileExists(sname.c_str(), true))
    {
      this->ShowPhotoSphere = sname;
    }
    else
    {
      vtkGenericWarningMacro("TourStop found referencing missing photosphere " << sname);
    }
  }
  const char *audioFile = poseel->GetAttribute("AudioFile");
  if (audioFile)
  {
    std::string sname = rootDir + "/" + std::string(audioFile);
    if (vtksys::SystemTools::FileExists(sname.c_str(), true))
    {
      this->AudioFile = sname;
    }
    else
    {
      vtkGenericWarningMacro("TourStop found referencing missing audio file " << sname);
    }
  }

  itmp = -1;
  poseel->GetScalarAttribute("CameraPose", itmp);
  for (auto &pose : cameraPoses)
  {
    if (itmp == pose.PoseNumber)
    {
      this->CameraPose = &(pose);
    }
  }
}
